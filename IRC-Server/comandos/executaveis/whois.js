var comandoPassado
module.exports = new function (){
    this.command = "whois"
    this.execute = function (args,socket,clients){
        comandoPassado = args[0].trim()
        if(comandoPassado.toLowerCase() != this.command){
            this.executed = false
            return
        }
        run(args,socket)
        this.executed = true
    }
}

function run(args,socket){
    socket.write("erro: comando "+comandoPassado+" ainda não implementado!\n")
}

