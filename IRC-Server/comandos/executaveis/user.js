var comandoPassado
module.exports = new function (){
    this.command = "user"
    this.execute = function (args,socket,clients){
        comandoPassado = args[0].trim()
        if(comandoPassado.toLowerCase() != this.command){
            this.executed = false
            return
        }
        run(args,socket)
        this.executed = true
    }
}

function run(args,socket){
    if(precisaMaisParametros(args)) {
        socket.write('Parametros faltando');
    } else {
        cadastraUsuario(args, socket);
    }
}

function precisaMaisParametros(args) {
    return !args[1] || !args[2] || !args[3] || !args[4] || !args[5]  
}

function cadastraUsuario(args, socket) {
    let realName = args.slice(5, args.length).join(' ');
    let userMode = args[2];
    let nickname = args[1];

    socket.realName = realName;
    socket.userMOde = userMode;

    socket.write('Usuário '+ realName + ' cadastrado com sucesso');
}