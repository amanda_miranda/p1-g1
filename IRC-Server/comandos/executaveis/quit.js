var comandoPassado
module.exports = new function (){
    this.command = "quit"
    this.execute = function (args,socket,clients){
        comandoPassado = args[0].trim()
        if(comandoPassado.toLowerCase() != this.command){
            this.executed = false
            return
        }
        run(args,socket,clients);
        this.executed = true
    }
}

function run(args,socket,clients){
    quit(args,socket,clients);
}

function quit(args,socket,clients) {
    let mensagem = args.slice(1, args.length).join(' ');
    clients.broadcast(mensagem, socket);
    socket.end();
}

