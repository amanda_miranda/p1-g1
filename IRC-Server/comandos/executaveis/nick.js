var comandoPassado;

module.exports = new function (){
    this.command = "nick"
    this.execute = function (args,socket,clients){
        comandoPassado = args[0].trim()
        if(comandoPassado.toLowerCase() != this.command){
            this.executed = false
            return
        }
        run(args,socket,clients)
        this.executed = true
    }
}

function run(args,socket,clients){

    if (!existeArgumentoNick(args)) {
        socket.write("Ops: Falta o nickname!\n")
        return
    } else if (existeNickName(args,clients)) {
        socket.write("Ops: Já existe esse nickname!\n")
        return
    } else {
        cadastrarNick(args,socket)
    }
    
    socket.write("OK: comando "+comandoPassado+" executado\n")
    
}

function existeArgumentoNick(args){
    return args[1] != undefined && args[1] != ""
}

function existeNickName(args,clients){
    let nick = args[1].toLowerCase();
    return clients.sockets.find(socket => socket.nick == nick);
}

function cadastrarNick(args, socket) {
    var nick = args[1];
    socket.nick = nick.toLowerCase();
}
