var comandoPassado
module.exports = new function (){
    this.command = "join"
    this.execute = function (args,socket,clients){
        comandoPassado = args[0].trim()
        if(comandoPassado.toLowerCase() != this.command){
            this.executed = false
            return
        }
        run(args,socket,clients)
        this.executed = true
    }
}

function run(args,socket,clients){
    if(precisaMaisParametros(args)) {
        socket.write('Parametros faltando');
    } else{
        joinChat(args,socket,clients);
    }
}
function precisaMaisParametros(args){
    return !args[1];
}
function joinChat(args,socket,clients){

    let nomeCanal = args[1].toLowerCase();
    let canal = clients.canais.find(c => c.nome == nomeCanal);
    if(canal){
        canal.usuarios.push(socket.nick);
        return;
    }

    let usuarios = [socket.nick];
    canal = {
            nome : nomeCanal,
            usuarios : usuarios
    }
    clients.canais.push(canal);
}
