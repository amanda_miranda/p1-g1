
module.exports = new function(){
    var comandList = []
    comandList.push(require("./executaveis/nick.js"))
    comandList.push(require("./executaveis/user.js"))
    comandList.push(require("./executaveis/join.js"))
    comandList.push(require("./executaveis/who.js"))
    comandList.push(require("./executaveis/whois.js"))
    comandList.push(require("./executaveis/part.js"))
    comandList.push(require("./executaveis/quit.js"))
    comandList.push(require("./executaveis/topic.js"))
    comandList.push(require("./executaveis/names.js"))
    comandList.push(require("./executaveis/list.js"))
    comandList.push(require("./executaveis/privmsg.js"))
    comandList.push(require("./executaveis/ping.js"))
    comandList.push(require("./executaveis/pong.js"))
    comandList.push(require("./executaveis/motd.js"))
    comandList.push(require("./executaveis/comandoNaoSuportado.js"))// O comando não suportado sempre deve ser o ultimo da lista.
    
    this.execute = function(data,socket,clients){
        var args = String(data).split(" ")
        for (i = 0; i < comandList.length; i++) { 
            comandList[i].execute(args,socket,clients);
            if(comandList[i].executed){
                break
            }
        }
    }
}
