// Load the TCP Library
var porta = 6667
net = require('net');

// Keep track of the chat clients
var clients = {
  sockets : [],
  canais : [],
  broadcast : broadcast
};

comandos = require("./comandos/comandos.js");

// Start a TCP Server
net.createServer(function (socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 

  // Put this new client in the list
  clients.sockets.push(socket);

  // Send a nice welcome message and announce
  socket.write("Welcome " + socket.name + "\n");
  clients.broadcast(socket.name + " joined the chat\n", socket);

  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    comandos.execute(data,socket,clients);
  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.sockets.splice(clients.sockets.indexOf(socket), 1);
    clients.broadcast(socket.name + " left the chat.\n");
  });
}).listen(porta);

// Send a message to all clients
function broadcast(message, sender) {
  clients.sockets.forEach(function (socket) {
    // Don't want to send it to sender
    if (socket === sender) return;
    socket.write(message);
  });
  // Log it to the server output too
  process.stdout.write(message)
}

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port "+porta+"\n");
